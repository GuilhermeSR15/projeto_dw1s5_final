package dw1s5.bd;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;

public class ConexaoBD {

	private BasicDataSource dataSource;
	private String usuario = "root"; 
	private String senha = "toor"; 
	
	//MySQL
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/projeto";
	
	//ORACLE
	//private String driver = "oracle.jdbc.OracleDriver";
	//private String url = "jdbc:oracle:thin:@localhost:1521:XE";

	private static ConexaoBD conexaoBD = new ConexaoBD();

	private ConexaoBD() {
		dataSource = new BasicDataSource();
		dataSource.setUsername(usuario);
		dataSource.setPassword(senha);
		dataSource.setUrl(url);
		dataSource.setDriverClassName(driver);
	}

	public static ConexaoBD getInstance() {
		return conexaoBD;
	}

	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
}
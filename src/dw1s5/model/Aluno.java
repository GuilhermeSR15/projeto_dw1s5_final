package dw1s5.model;

import java.time.LocalDate;
import java.util.List;

public class Aluno extends Usuario {
	
	private List<Disciplina> disciplinas;
	private LocalDate dataNasc;
	private boolean validado;
	//private File foto;
	
	public Aluno() { /* construtor vazio */ }
	
	public Aluno(String cpf, String nome, LocalDate dataNasc, String email, String senha) {
		super(cpf, nome, email, senha, 2);
		this.dataNasc = dataNasc;
	}

	public String getCpf() {
		return super.getCpf();
	}

	public void setCpf(String cpf) {
		super.setCpf(cpf);;
	}

	public String getNome() {
		return super.getNome();
	}

	public void setNome(String nome) {
		super.setNome(nome);
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public String getEmail() {
		return super.getEmail();
	}

	public void setEmail(String email) {
		super.setEmail(email);;
	}

	public String getSenha() {
		return super.getSenha();
	}

	public void setSenha(String senha) {
		super.setSenha(senha);
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
	
	
	public boolean isMatriculado(Disciplina disciplina) {
		for(Disciplina d : disciplinas) {
			if(d.getCodigo() == disciplina.getCodigo()) {
				return true;
			}
		}
		
		return false;
	}
	
	
}

package dw1s5.model;

public class Disciplina {

	private String nome;
	private int codigo;
	private int maxAlunos;

	public Disciplina() {
		/* construtor vazio */ }

	public Disciplina(String nome, int codigo, int maxAlunos) {
		super();
		this.nome = nome;
		this.codigo = codigo;
		this.maxAlunos = maxAlunos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getMaxAlunos() {
		return maxAlunos;
	}

	public void setMaxAlunos(int maxAlunos) {
		this.maxAlunos = maxAlunos;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Disciplina) {
				if(((Disciplina)obj).getCodigo() == this.codigo) {
					return true;
				}
			}
		}
		return false;
	}

}

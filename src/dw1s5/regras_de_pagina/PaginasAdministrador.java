package dw1s5.regras_de_pagina;

public enum PaginasAdministrador {
	HOME("/homeadmin.jsp"),
	LOGOUT("/logout.jsp"),
	ERRO("/WEB-INF/erro.jsp");

	private String url;

	private PaginasAdministrador(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public static boolean valido(String req) {
		boolean valido = false;
		for (PaginasAdministrador s : PaginasAdministrador.values()) {
			if (req.equals(s.getUrl())) {
				valido = true;
			}
		}
		return valido;
	}
}

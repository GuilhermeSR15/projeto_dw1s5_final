package dw1s5.regras_de_pagina;

public enum PaginasSemValidacao {

	LOGIN("/index.jsp"),
	CADASTRAR("/cadastro.jsp"),
	MENU("/WEB-INF/menu.jsp"),
	ERRO("/WEB-INF/erro.jsp");
	

	private String url;

	private PaginasSemValidacao(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public static boolean valido(String req) {
		boolean valido = false;
		for (PaginasSemValidacao s : PaginasSemValidacao.values()) {
			if (req.equals(s.getUrl())) {
				valido = true;
			}
		}
		return valido;
	}
}

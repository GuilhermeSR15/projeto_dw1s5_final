package dw1s5.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw1s5.model.Administrador;
import dw1s5.regras_de_pagina.PaginasAdministrador;
import dw1s5.regras_de_pagina.PaginasSemValidacao;

/**
 * Servlet Filter implementation class FiltroAutenticacao
 */
@WebFilter("/*")
public class FiltroAutenticacao implements Filter {

	/**
	 * Default constructor.
	 */
	public FiltroAutenticacao() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);

		String uri = req.getServletPath();
		String operacao = request.getParameter("acao");

		if (uri.endsWith(".css") || uri.endsWith(".js")) {
			chain.doFilter(request, response);
		} else {
			try {
				filtrandoAdministrador(request, response, chain, req, res, session, uri);
			} catch (Exception e) {
				filtrando(request, response, chain, req, res, session, uri, operacao);
			}
		}
	}

	private void filtrandoAdministrador(ServletRequest request, ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String uri)
			throws IOException, ServletException {

		Administrador administrador = (Administrador) session.getAttribute("usuario");

		if (administrador == null) {
			throw new RuntimeException();
		} else if (PaginasAdministrador.valido(uri)) {
			chain.doFilter(request, response);
		} else {
			RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/erro.jsp");
			dispatcher.forward(req, res);
		}
	}

	private void filtrando(ServletRequest request, ServletResponse response, FilterChain chain, HttpServletRequest req,
			HttpServletResponse res, HttpSession session, String uri, String operacao)
			throws IOException, ServletException {
		System.out.println(uri);
		boolean paginaSemAut = PaginasSemValidacao.valido(uri);
		boolean autenticado = (session != null && session.getAttribute("usuario") != null);
		boolean comandoLogin = "Login".equals(operacao) || "Cadastrar".equals(operacao);

		if (autenticado || paginaSemAut || comandoLogin) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect(req.getContextPath() + "/index.jsp");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}

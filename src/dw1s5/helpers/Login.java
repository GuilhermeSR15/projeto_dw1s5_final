package dw1s5.helpers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw1s5.daos.AdministradorDAO;
import dw1s5.daos.AlunoDAO;
import dw1s5.model.Aluno;
import dw1s5.model.Usuario;
import dw1s5.regras_de_pagina.PaginasAdministrador;

public class Login implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String email = request.getParameter("login");
		String senha = request.getParameter("senha");
		HttpSession session = request.getSession();

		AlunoDAO alunoDao = new AlunoDAO();

		Usuario aluno = alunoDao.autenticacao(email, senha);

		List<Aluno> alunosSemAuth = alunoDao.listarAlunoSemAuth();

		if (aluno != null) {
			session.setAttribute("usuario", aluno);
			return "/home.jsp";
		} else {
			AdministradorDAO admDao = new AdministradorDAO();
			Usuario adm = admDao.autenticacao(email, senha);
			if (adm != null) {
				session.setAttribute("usuario", adm);
				request.setAttribute("lista", alunosSemAuth);
				return PaginasAdministrador.HOME.getUrl();
			} else {
				request.setAttribute("erro", "E-mail ou senha invalidos!");
				return "/WEB-INF/erro.jsp";
			}
		}
	}
}
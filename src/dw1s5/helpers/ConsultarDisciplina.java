package dw1s5.helpers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dw1s5.daos.DisciplinaDAO;
import dw1s5.model.Aluno;
import dw1s5.model.Disciplina;
import dw1s5.model.Usuario;

public class ConsultarDisciplina implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Aluno aluno = (Aluno) request.getSession(false).getAttribute("usuario");
		DisciplinaDAO dao = new DisciplinaDAO();
		dao.getDisciplinas(aluno);
		List<Disciplina> disciplinasDisponiveis = dao.getDisciplinasDisponiveis(); 
		List<Disciplina> disciplinas = dao.listarDisciplinas(); 
		
		disciplinas.removeAll(disciplinasDisponiveis);
		disciplinasDisponiveis.removeAll(aluno.getDisciplinas());
		disciplinas.removeAll(aluno.getDisciplinas());
		
		request.setAttribute("disciplinas", disciplinas);
		request.setAttribute("disciplinasDisponiveis", disciplinasDisponiveis);
		request.setAttribute("aluno", aluno);
		return "/WEB-INF/disciplinas.jsp";
	}

}

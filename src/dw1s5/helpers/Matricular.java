package dw1s5.helpers;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw1s5.daos.AdministradorDAO;
import dw1s5.daos.AlunoDAO;
import dw1s5.daos.DisciplinaDAO;
import dw1s5.model.Aluno;
import dw1s5.model.Usuario;
import dw1s5.regras_de_pagina.PaginasAdministrador;

public class Matricular implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Aluno aluno = (Aluno) request.getSession(false).getAttribute("usuario");
		String[] disciplinas = request.getParameterValues("disciplina");
		DisciplinaDAO dao = new DisciplinaDAO();
		int inseriu = 0;
		boolean erro = false;
		try {
			for (String string : disciplinas) {
				inseriu = dao.cadastrarDisciplina(aluno, string);
				if(inseriu == 0) {
					erro = true;
				}
			}
		} catch (Exception e) {
			inseriu = 0;
		}
		if (erro) {
			request.setAttribute("erro", "Erro ao cadastrar ao banco de dados!");
		}
		return new ConsultarDisciplina().executa(request, response);
	}
}
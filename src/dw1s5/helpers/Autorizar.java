package dw1s5.helpers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dw1s5.daos.AlunoDAO;
import dw1s5.model.Aluno;

public class Autorizar implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String cpf = request.getParameter("checkbox");
		System.out.println("veio no autorizar");
		AlunoDAO alunoDao = new AlunoDAO();
		int i = alunoDao.autorizarAluno(cpf);
		if (i != 0)
			return "/homeadmin.jsp";
		request.setAttribute("erro", "N�o foi poss�vel alterar a senha!");
		return "/erro.jsp";
	}

	
}

package dw1s5.helpers;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Home implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sair = request.getParameter("Logout");
		String consultar = request.getParameter("Consultar Disciplinas");
		String alterar= request.getParameter("Alterar Senha");
		HttpSession session = request.getSession(false);
		if(sair != null) {
			session.invalidate();
			return "/logout.jsp";
		} else if(consultar != null) {
			return "/consultar.jsp";
		} else if(alterar != null) {
			System.out.println("AAAAAAAALTERAAA");
			return "/alterar.jsp";
		} else {
			return "/home.jsp";
		}
	}
}

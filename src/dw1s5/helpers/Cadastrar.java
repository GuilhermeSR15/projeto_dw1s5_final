package dw1s5.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dw1s5.daos.AlunoDAO;
import dw1s5.model.Aluno;

public class Cadastrar implements Comando {
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 50 * 1024;
	private int maxMemSize = 4 * 1024;
	private File file;

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String cpf = request.getParameter("cpf");
		String nome = request.getParameter("nome");
		String senha = request.getParameter("senha");
		String email = request.getParameter("email");
		String dataNasc = request.getParameter("data");
		Aluno aluno;
		AlunoDAO alunoDao = new AlunoDAO();
		int inseriu = 0;
		if (cpf == null) {
			return "/cadastro.jsp";
		} else {
			try {
				aluno = new Aluno(cpf, nome, LocalDate.parse(dataNasc), email, senha);
				inseriu = alunoDao.salvar(aluno);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (inseriu == 0) {
				request.setAttribute("erro", "Erro ao cadastrar ao banco de dados!");
				return "/cadastro.jsp";
			}
			request.setAttribute("success", "Aluno cadastrado com sucesso!");
			return "/index.jsp";
		}

	}
}

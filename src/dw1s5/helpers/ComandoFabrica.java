package dw1s5.helpers;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class ComandoFabrica {
	
	public Comando getComando(HttpServletRequest request, ServletContext contexto) {
		String operacao = request.getParameter("acao");
		String nomeClasse = contexto.getInitParameter(operacao);
		try {
			Class<?> clazz = Class.forName(nomeClasse);
			Comando comando = (Comando)clazz.newInstance();
			return comando;
		}
		catch(Exception erro) {
			throw new RuntimeException("Opera��o inv�lida", erro);
		}
	}
}


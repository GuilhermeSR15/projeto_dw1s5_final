package dw1s5.helpers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dw1s5.daos.AlunoDAO;
import dw1s5.model.Aluno;

public class AlterarSenha implements Comando {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String senhaNova = request.getParameter("senha-nova");
		String senhaNovaConfirmacao = request.getParameter("conf-senha-nova");

		if(senhaNova == null) {
			return "/alterar.jsp";
		} else {
			AlunoDAO alunoDao = new AlunoDAO();
			Aluno aluno = (Aluno) request.getSession(false).getAttribute("usuario");
			int i = alunoDao.alterarSenha(senhaNova, aluno);
			if (i != 0)
				return "/home.jsp";
			request.setAttribute("erro", "N�o foi poss�vel alterar a senha!");
			return "/erro.jsp";
		}
	}
}

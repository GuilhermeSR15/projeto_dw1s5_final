package dw1s5.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dw1s5.bd.ConexaoBD;
import dw1s5.model.Administrador;
import dw1s5.model.Aluno;

public class AdministradorDAO {

	public List<Administrador> listarAdministradores() throws SQLException {
		List<Administrador> lista = new ArrayList<>();
		Administrador adm = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection(); ResultSet rSet = getBuscarPS(con).executeQuery();) {
			while (rSet.next()) {
				adm = new Administrador(rSet.getString("cpf"), rSet.getString("email"), rSet.getString("senha"),
						rSet.getString("nome"), 1);
				lista.add(adm);
				System.out.println(adm);
			}
		}
		return lista;
	}

	public Administrador buscarAdministrador(String email) throws SQLException {
		Administrador adm = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection(); ResultSet rSet = getBuscarPS(con, email).executeQuery();) {
			while (rSet.next()) {
				adm = new Administrador(rSet.getString("cpf"), rSet.getString("email"), rSet.getString("senha"),
						rSet.getString("nome"), 1);
				System.out.println(adm);
			}
		}
		return adm;
	}

	private PreparedStatement getBuscarPS(Connection con, String email) throws SQLException {
		String sql = "select * from administradores where cpf like ?";
		PreparedStatement pStat = con.prepareStatement(sql);
		pStat.setString(1, email);
		return pStat;
	}

	private PreparedStatement getBuscarPS(Connection con) throws SQLException {
		String sql = "select * from administradores";
		PreparedStatement pStat = con.prepareStatement(sql);
		return pStat;
	}
	
	public Administrador autenticacao(String email, String senha) throws SQLException {
		Administrador administrador = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "select cpf, nome, email from administradores where senha = ? and email = ?";
		try (Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql)) {
			pStat.setString(1, senha);
			pStat.setString(2, email);
			
			try( ResultSet rSet = pStat.executeQuery()){
			
				if (rSet.next()) {
					administrador = new Administrador(rSet.getString("cpf"), rSet.getString("nome"),
							rSet.getString("email"), null);
				}
			}
		}
		return administrador;
	}
}

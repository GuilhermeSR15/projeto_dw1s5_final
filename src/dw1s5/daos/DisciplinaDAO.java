package dw1s5.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import dw1s5.bd.ConexaoBD;
import dw1s5.model.Aluno;
import dw1s5.model.Disciplina;

public class DisciplinaDAO {

	public List<Disciplina> listarDisciplinas() throws SQLException {
		List<Disciplina> lista = new ArrayList<>();
		Disciplina disciplina = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection(); ResultSet rSet = getBuscarPS(con).executeQuery();) {
			while (rSet.next()) {
				disciplina = new Disciplina(rSet.getString("nome"), rSet.getInt("codigo"), rSet.getInt("max_alunos"));
				lista.add(disciplina);
				System.out.println(disciplina);
			}
		}
		return lista;
	}

	public Disciplina buscarDisciplina(int codigo) throws SQLException {
		Disciplina disciplina = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection(); ResultSet rSet = getBuscarPS(con, codigo).executeQuery();) {
			while (rSet.next()) {
				disciplina = new Disciplina(rSet.getString("nome"), rSet.getInt("codigo"), rSet.getInt("max_alunos"));
				System.out.println(disciplina);
			}
		}
		return disciplina;
	}

	private PreparedStatement getBuscarPS(Connection con, int codigo) throws SQLException {
		String sql = "select * from disciplina where codigo like ?";
		PreparedStatement pStat = con.prepareStatement(sql);
		pStat.setInt(1, codigo);
		return pStat;
	}

	private PreparedStatement getBuscarPS(Connection con) throws SQLException {
		String sql = "select * from disciplina";
		PreparedStatement pStat = con.prepareStatement(sql);
		return pStat;
	}

	public Aluno getDisciplinas(Aluno aluno) throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		List<Disciplina> lista = new ArrayList<>();
		String sql = "select d.nome, d.codigo, d.max_alunos from disciplina d join aluno_disciplina a on a.disciplina = d.codigo "
				+ "where aluno = ?";
		try (Connection con = conexao.getConnection(); PreparedStatement pStat = con.prepareStatement(sql)) {
			pStat.setString(1, aluno.getCpf());

			try (ResultSet rSet = pStat.executeQuery()) {

				while (rSet.next()) {
					lista.add(new Disciplina(rSet.getString("nome"), rSet.getInt("codigo"), rSet.getInt("max_alunos")));
				}
			}
		}
		aluno.setDisciplinas(lista);
		return aluno;
	}

	public List<Disciplina> getDisciplinasDisponiveis() throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		List<Disciplina> lista = new ArrayList<>();
		String sql = "select * from disciplina d where max_alunos > "
				+ "(select count(*) from aluno_disciplina a where a.disciplina = d.codigo)";
		try (Connection con = conexao.getConnection();
				PreparedStatement pStat = con.prepareStatement(sql);
				ResultSet rSet = pStat.executeQuery()) {

			while (rSet.next()) {
				lista.add(new Disciplina(rSet.getString("nome"), rSet.getInt("codigo"), rSet.getInt("max_alunos")));
			}
		}
		return lista;
	}

	public int cadastrarDisciplina(Aluno aluno, String codigoDisc) throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		int inseriu = 0;

		String sql = "INSERT INTO aluno_disciplina(aluno, disciplina, data_matricula) values (?, ?, ?)";

		try (Connection con = conexao.getConnection(); PreparedStatement pStat = con.prepareStatement(sql);) {
				pStat.setString(1, aluno.getCpf());
				pStat.setString(2, codigoDisc);
				pStat.setDate(3, Date.valueOf(LocalDate.now()));

			inseriu = pStat.executeUpdate();
		}
		return inseriu;
	}

}

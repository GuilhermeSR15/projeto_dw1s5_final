package dw1s5.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dw1s5.bd.ConexaoBD;
import dw1s5.model.Aluno;

public class AlunoDAO {
	
	public int salvar(Aluno a) throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "insert into aluno values (?,?,?,?,?,?,?)";
		int inseriu = 0;
		try(
			Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql);
		) {
			pStat.setString(1, a.getCpf());
			pStat.setString(2, a.getNome());
			pStat.setDate(3, Date.valueOf(a.getDataNasc()));
			pStat.setString(4, a.getEmail());
			pStat.setString(5, a.getSenha());
			pStat.setInt(6, a.getPermissao());
			pStat.setInt(7, (a.isValidado()? 1:0));			
			inseriu = pStat.executeUpdate();
		}
		return inseriu;
	}
	
	public int alterarSenha(String senha, Aluno a) throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "update aluno set senha = ? where cpf like ?";
		int inseriu = 0;
		try(
			Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql);
		) {
			pStat.setString(1, senha);
			pStat.setString(2, a.getCpf());		
			inseriu = pStat.executeUpdate();
		}
		return inseriu;
	}

	public List<Aluno> listarAlunos() throws SQLException {
		List<Aluno> lista = new ArrayList<>();
		Aluno aluno = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection(); ResultSet rSet = getBuscarPS(con).executeQuery();) {
			while (rSet.next()) {
				aluno = new Aluno(rSet.getString("cpf"), rSet.getString("nome"),
						rSet.getDate("data_nascimento").toLocalDate(), rSet.getString("email"), null);
				lista.add(aluno);
				System.out.println(aluno);
			}
		}
		return lista;
	}

	public Aluno buscarAluno(String cpf) throws SQLException {
		Aluno aluno = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		try (Connection con = conexao.getConnection();ResultSet rSet =  getBuscarPS(con, cpf).executeQuery()) {
			while (rSet.next()) {
				aluno = new Aluno(rSet.getString("cpf"), rSet.getString("nome"),
						rSet.getDate("data_nascimento").toLocalDate(), rSet.getString("email"), null);
				System.out.println(aluno);
			}
		}
		return aluno;
	}

	private PreparedStatement getBuscarPS(Connection con, String cpf) throws SQLException {
		String sql = "select * from aluno where coluna like ?";
		PreparedStatement pStat = con.prepareStatement(sql);
		pStat.setString(1, cpf);
		return pStat;
	}
	
	private PreparedStatement getBuscarPS(Connection con) throws SQLException {
		String sql = "select * from aluno";
		PreparedStatement pStat = con.prepareStatement(sql);
		return pStat;
	}
	
	public List<Aluno> listarAlunoSemAuth() throws SQLException {
		List<Aluno> aluno = new ArrayList<>();
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "select * from aluno where validado = 0";
		try (Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql)) {
			try( ResultSet rSet = pStat.executeQuery()){
				while (rSet.next()) {
					aluno.add(new Aluno(rSet.getString("cpf"), rSet.getString("nome"),
							rSet.getDate("data_nascimento").toLocalDate(), rSet.getString("email"), null));
				}
			}
		}
		return aluno;
	}
	
	public Aluno autenticacao(String email, String senha) throws SQLException {
		Aluno aluno = null;
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "select cpf, nome, data_nascimento, email from aluno where senha = ? and email = ? and validado = 1";
		try (Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql)) {
			pStat.setString(1, senha);
			pStat.setString(2, email);
			
			try( ResultSet rSet = pStat.executeQuery()){
			
				while (rSet.next()) {
					aluno = new Aluno(rSet.getString("cpf"), rSet.getString("nome"),
							rSet.getDate("data_nascimento").toLocalDate(), rSet.getString("email"), null);
				}
			}
		}
		return aluno;
	}
	
	public int autorizarAluno(String cpf) throws SQLException {
		ConexaoBD conexao = ConexaoBD.getInstance();
		String sql = "update aluno set validado = 1 where cpf like ?";
		int inseriu = 0;
		try(
			Connection con = conexao.getConnection();
			PreparedStatement pStat = con.prepareStatement(sql);
		) {
			pStat.setString(1, cpf);
			inseriu = pStat.executeUpdate();
		}
		return inseriu;
	}

}

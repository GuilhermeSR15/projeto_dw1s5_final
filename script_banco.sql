use projeto;

create table disciplina (
    codigo int primary key
   ,nome varchar(255)
   ,max_alunos int
);


create table administradores (
    cpf varchar(12) primary key
   ,nome varchar(255)
   ,email varchar(255) unique
   ,senha varchar(16)
);

create table aluno (
     cpf varchar(12) primary key
   ,nome varchar(255)
   ,data_nascimento date
   ,email varchar(255) unique
   ,senha varchar(16)
   ,validado int(1) not null check (validado in(0,1))
   -- foto no banco???
);


create table aluno_disciplina (
    aluno varchar(12)
   ,disciplina int
   ,data_matricula date
   ,constraint discp_fk 
   foreign key (disciplina)
   references disciplina(codigo)
   ,constraint aluno_fk 
   foreign key (aluno)
   references aluno(cpf)
);

insert into disciplina values (1, 'Desenvolvimento Web', 20);
insert into disciplina values (2, 'Desenvolvimento Mobile', 20);
insert into disciplina values (3, 'Redes', 20);
insert into disciplina values (4, 'Metodologia', 20);
insert into disciplina values (5, 'Lógica de Programação', 20);

insert into administradores values ('123456789010', 'Admin Teste', 'a@a', '1');
insert into administradores values ('123456789011', 'Ednilson', 'ednilson@ifsp.edu.br', '123456', 1);
insert into administradores values ('123456789012', 'Denis', 'denis@ifsp.edu.br', '123456', 1);
insert into administradores values ('123456789013', 'Fernando', 'fernando@ifsp.edu.br', '123456', 1);
insert into administradores values ('123456789014', 'Fabio', 'fabio@ifsp.edu.br', '123456', 1);
insert into administradores values ('123456789015', 'Gislaine', 'gislaine@ifsp.edu.br', '123456', 1);

desc administradores;

select * from aluno;

insert into aluno values ('123456789030', 'Teste 2', sysdate, 'teste@2.com', '123456', 2, 0);
insert into aluno values ('123456789020', 'Teste 1', sysdate, 'teste@1.com', '123456' 2, 1);
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Disciplina</title>
<link rel="stylesheet" type="text/css" href="css/menu.css">
<link rel="stylesheet" type="text/css" href="css/homeAdm.css">
<link rel="stylesheet" type="text/css" href="css/alterar.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">
</head>
<body>
	<form method="post" action="Servlet">
		<jsp:include page="/WEB-INF/menu.jsp"></jsp:include>

		<section class="conteudo">
		<h1>Lista de disciplinas</h1>
		
		<c:if test="${erro != null}">
			<p>${erro}</p>
		</c:if>
		
		<table>
			<thead>
				<tr>
					<th>Matricular?</th>
					<th>Código</th>
					<th>Nome</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="disciplina" items="${disciplinas}"
					varStatus="contador">
					<tr>
						<td>Sem Vaga</td>
						<td>${disciplina.codigo}</td>
						<td>${disciplina.nome}</td>
					</tr>
				</c:forEach>
				<c:forEach var="disciplina" items="${aluno.disciplinas}"
					varStatus="contador">
					<tr>
						<td>Matriculado</td>
						<td>${disciplina.codigo}</td>
						<td>${disciplina.nome}</td>
					</tr>
				</c:forEach>
				<c:forEach var="disciplina" items="${disciplinasDisponiveis}"
					varStatus="contador">
					<tr>
						<td><input type="checkbox" name="disciplina"
							value="${disciplina.codigo}" /></td>
						<td>${disciplina.codigo}</td>
						<td>${disciplina.nome}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<input type="submit" name="acao" value="Matricular"> </section>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<title>Cadastro</title>
	</head>
	<body>
	   <section>
	        <form method="post"
                  action="Servlet"
                  novalidate="novalidate"
                  onsubmit="return validar()"
                  id="form" enctype='multipart/form-data'>
                  
                  
                <label class="label" for="nome">
                    Nome
                </label>
                <input type="text" name="nome" placeholder="Nome" required="required">
                <label class="label" for="login">
                    E-mail
                </label>
                <input type="email" name="email" placeholder="E-mail" required="required">
                <label class="label" for="senha">
                    Senha
                </label>
                <input type="password" name="senha" placeholder="Senha" required="required">
                <label class="label" for="senha">
                    Confirme a senha
                </label>
                <input type="password" name="senha" placeholder="Senha" required="required">
                <label class="label" for="cpf">
                    CPF
                </label>
                <input type="text" name="cpf" placeholder="CPF" pattern="\d{11}" required="required" title="Informe somente números">
                <label class="label" for="data">
                    Data de Nascimento
                </label>
                <input type="date" name="data" required="required"  min="1918-01-01" >
                <label class="label" for="foto">
                    Foto
                </label>
                <input type="file" name="foto" accept="image/*">
                
                <input type="submit" name="acao" value="Cadastrar">
                <input type="reset" name="acao" value="Apagar">
            </form>
	   </section>
	   <script type="text/javascript" src="js/validar_cadastro.js"></script>
	</body>
</html>
function validar() {
    var senhaNova = document.getElementById("senha-nova").value;
    var confSenhaNova = document.getElementById("conf-senha-nova").value;

    if (senhaNova != confSenhaNova) {
        alert("Senhas não conferem!" + senhaNova+confSenhaNova);
        return false;
    }

    return true;
}

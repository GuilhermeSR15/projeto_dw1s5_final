function validar() {
    var elementos;
    var erros = " ";
    var form = document.getElementById("form");
    var contadorErros = 0;

    elementos = form.elements;

    for(var i = 0; i < elementos.length; i++) {
        if(!elementos[i].validity.valid){
            erros += "\nValor incorreto para " + elementos[i].name;
            contadorErros++;
        }
    }

    if (contadorErros > 0) {
        alert(erros);
        return false;
    }

    return true;
}

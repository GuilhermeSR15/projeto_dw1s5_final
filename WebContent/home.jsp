<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Home - ${usuario.nome}</title>
		<link rel="stylesheet" type="text/css" href="css/menu.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	</head>
	<body>
	   <form action="Servlet"
	         method="post"> 
	       <jsp:include page="WEB-INF/menu.jsp"></jsp:include>
	       <div class="dados">
	            <p>CPF: ${usuario.cpf}</p>
	            <p>Nome: ${usuario.nome}</p>
	            <p>Data de Nascimento: ${usuario.dataNasc}</p>
	            <p>E-mail: ${usuario.email}</p>         
           </div>
       </form>
	</body>
</html>
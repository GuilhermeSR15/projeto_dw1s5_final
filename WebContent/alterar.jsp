<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Alterar Senha</title>
		<link rel="stylesheet" type="text/css" href="css/menu.css">
		<link rel="stylesheet" type="text/css" href="css/alterar.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	</head>
	<body>
        <form action="Servlet"
              method="post" 
              novalidate="novalidate"
              onsubmit="return validar()"
              id="form">
              
            <jsp:include page="WEB-INF/menu.jsp"></jsp:include>
            <section class="conteudo">
	            <label for="senha-nova">Nova senha</label>
	            <input type="password" name="senha-nova" id="senha-nova" placeholder="Nova senha">
	            <label for="conf-nova-senha">Confirme a nova senha</label>
	            <input type="password" name="conf-nova-senha" id="conf-senha-nova" placeholder="Confirme a nova senha">
	            <input type="submit" name="acao" value="Alterar Senha">
	            <input type="reset" name="acao" value="Apagar">
            </section>
        </form>	
        <script type="text/javascript" src="js/validar_senha.js"></script>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
	</head>
	<body>
	    <p>${mensagem}</p>
	    <a href="index.jsp">Voltar à página inicial</a>
	</body>
</html>
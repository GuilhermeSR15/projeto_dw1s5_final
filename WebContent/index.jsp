<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/index.css"> 
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    </head>
    <body>
        <section>
            <form method="post"
                  action="Servlet">
                <h1>Bem Vindo ao SUAP</h1>
	            <label class="label" for="login">
	                Login
	            </label>
	            <input type="email" name="login" placeholder="Login">
	             <label class="label" for="senha">
	                Senha
	            </label>
	            <input type="password" name="senha" placeholder="Senha">
	            <input type="submit" name="acao" value="Login">
	            <input type="reset" name="acao" value="Apagar">
	            <input type="submit" name="acao" value="Cadastrar">
            </form>
        </section>
    </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Home - ${usuario.nome}</title>
	    <link rel="stylesheet" type="text/css" href="css/menu.css">
        <link rel="stylesheet" type="text/css" href="css/homeAdm.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	</head>
	<body>
	   <form action="Servlet"
	         method="post">
	       <jsp:include page="WEB-INF/menu-adm.jsp"></jsp:include>
		   <table>
	           <tr>
		            <th>Nome</th>
		            <th>E-mail</th>
		            <th>CPF</th>
		            <th>Autorizar</th>
	           </tr>
                <c:forEach var="aluno" items="${lista}">
	                <tr>
	                    <td>${aluno.nome}</td>            
	                    <td>${aluno.email}</td>
	                    <td>${aluno.cpf}</td>
	                    <td><input type="checkbox" name="checkbox" value="${aluno.cpf}"></td>
	                </tr>
                </c:forEach>
           </table>
           <input type="submit" name="acao" value="Autorizar">
		</form>
	</body>
</html>